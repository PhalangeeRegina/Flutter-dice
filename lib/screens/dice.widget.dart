import "package:flutter/material.dart";
import 'package:audioplayers/audioplayers.dart';

import 'dart:math';

class DiceWidget extends StatefulWidget {
  @override
  _DiceWidgetState createState() => _DiceWidgetState();
}

class _DiceWidgetState extends State<DiceWidget> {
  int leftDiceIndex = 1;
  int rightDiceIndex = 5;
  final Random rand = new Random();

  changeIndex() {
    setState(() {
      AudioPlayer audioPlayer = new AudioPlayer();
      final player = AudioCache();
      player.play('audio.wav');
      leftDiceIndex = rand.nextInt(5) + 1;
      rightDiceIndex = rand.nextInt(5) + 1;
    });
  }

  @override
  Widget build(BuildContext ctx) {
    return Center(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            flex: 1,
            child: FlatButton(
              onPressed: changeIndex,
              child: Container(
                margin: EdgeInsets.all(10),
                child: Image(
                  image: AssetImage("images/dice-$leftDiceIndex.png"),
                ),
              ),
            ),
          ),
          Expanded(
            flex: 1,
            child: FlatButton(
              onPressed: changeIndex,
              child: Container(
                margin: EdgeInsets.all(10),
                child: Image(
                  image: AssetImage("images/dice-$rightDiceIndex.png"),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
