import "package:flutter/material.dart";

import "./screens/dice.widget.dart";

void main() {
  runApp(DiceApp());
}

class DiceApp extends StatelessWidget {
  @override
  Widget build(BuildContext ctx) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Center(
            child: Text("Rolly Dice"),
          ),
          backgroundColor: Colors.teal[900],
        ),
        backgroundColor: Colors.teal[600],
        body: DiceWidget(),
      ),
    );
  }
}
